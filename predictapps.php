<?php
header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
header('Content-Type: application/json');

$url = "http://admin.utes.mobi/api/collections/get/PredictApps";
$token = "5854c8421361dd2704dde9102affc5";

$options = array(
    'http' => array(
        'header'  => "Content-type: application/json\r\n",
        'method'  => 'GET'
    )
);
$getdata = http_build_query(
    array(
        'token' => $token,
        'filter' => array('Active' => true)
    )
);

$context = stream_context_create($options);
$result = file_get_contents($url."?".$getdata, false, $context);

if ($result === FALSE) { /* Handle error */ }

print_r($result);
