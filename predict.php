<?php
header("Access-Control-Allow-Origin: ".$_SERVER['HTTP_ORIGIN']);
header('Content-Type: application/json');

$token = "gLN8X/sGnGblD/Pbehz09MEkp0oYuLEc8NmHxTcJ9CfmIRuOkPVxsuyvTtv0MLQ5H2AqD0VQqEQTT3C40tLcJQ==";
$url = "https://ussouthcentral.services.azureml.net/workspaces/99a1dc5deb6243038666b8987a6877d0/services/910f104aa83b494c81aac238bdee0639/execute?api-version=2.0&details=true";

$name = $_GET["Name"];
$closePrice = $_GET["Price"];
// $todayDate = date("m/d/Y H:i");
$data = array(
	"Inputs" => 
		array('input1' => array(
			"ColumnNames" => array("Name", "Price"),
			"Values" => array(array($name, $closePrice))
		)),
	"GlobalParameters" => array( 'a' => 'a')
);

$options = array(
    'http' => array(
        'header'  => "Content-type: application/json\r\n"
        			."Authorization: Bearer ".$token."\r\n",
        'method'  => 'POST',
        'content' => json_encode($data)
    )
);

$context = stream_context_create($options);
$result = file_get_contents($url, false, $context);

if ($result === FALSE) { /* Handle error */ }

print_r($result);
