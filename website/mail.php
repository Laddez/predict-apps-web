<?php

function mail_utf8($to, $from, $subject, $message)
{
    $subject = '=?UTF-8?B?' . base64_encode($subject) . '?=';
 
    $headers  = "MIME-Version: 1.0\r\n"; 
    $headers .= "Content-type: text/plain; charset=utf-8\r\n";
    $headers .= "From: $from\r\n";
 
    return mail($to, $subject, $message, $headers);
}

$subject = 'Someone filled Contact form on CP website';
// $to = 'anastasiya.migal@tateeda.com';
$to = 'info@predictapps.com';
$from = 'no-reply@predictapps.com';
$msg = '';

if (isset($_POST['name'])) {
	$msg .= 'Name: '.$_POST['name'];
	$msg .= "\r\n";
}
if (isset($_POST['email'])) {
	$msg .= 'Email: '.$_POST['email'];
	$msg .= "\r\n";
}
if (isset($_POST['message'])) {
	$msg .= 'Message: '.$_POST['message'];
	$msg .= "\r\n";
}

$msg .= "\r\n";
$msg .= $_SERVER['HTTP_ORIGIN'];

if (isset($_POST['name']) && 
	isset($_POST['email'])) {
		mail_utf8($to, $from, $subject, $msg);
	} else{
		echo "You don't have access to this file.";
}
?>