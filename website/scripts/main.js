$(function() {
    var $body = $('body');
    var $mainNavbar = $('#main-navbar');
    var $navbarCollapse = $(".navbar-collapse");
    var mainNavbarHeight = $mainNavbar.outerHeight();

    // function topScroll() {
    //     var shouldFixed = $(window).scrollTop() > 50;
    //     $mainNavbar.toggleClass('fixed-top', shouldFixed);
    // }

    // topScroll();

    // $(window).scroll(function() {
    //     topScroll();
    // });

    $('[data-page-scroll]').bind('click', function (event) {
        var target = $(this).attr('data-page-scroll') || $(this).attr('href');
        var $target = $(target);
        var $navbar = $('#main-navbar');
        var $linkItem = $('#main-navbar li a');

        if (!$target.length) {
            window.location.href = '/' + target;
        }
        
        var scrollTo = $target.offset().top - mainNavbarHeight;

        if ($target.length) {
            $navbarCollapse.collapse('hide');
            
            $('html, body').stop().animate({
                scrollTop: (scrollTo)
            }, 1500, 'easeInOutExpo');
        }
        event.preventDefault();
        return false;
    });

    var currentLocation = window.location.href.match(/\/([\w-]+)\/?$/);
    if (currentLocation && currentLocation[1]) {
        var $target = $('.nav-link[href="'+ currentLocation[1] + '"]');

        if ($target.length) {
            $('.nav-link').removeClass('active');
            $('.nav-link[href="'+ currentLocation[1] + '"]').addClass('active');
        }
    }
    $('p.section-subtitle').widowFix();


    $.get("https://ipinfo.io", function(info) {

        var domain = window.location.hostname === 'localhost'
            ? 'http://predictapps.com'
            : '';
            
        $.get(domain + '/predictapps.php', function(result) {    
            window.currentCountry = info.country;
            window.predictApps = result.entries;
            modifyPredicAppIsAvailable(window.predictApps);

            // $('body').trigger('crypto.FillTable');
            $('body').trigger('crypto.GetData');
            fillPredictApps(6, info.country);

            // Adding dropdown with changing location
            // dropdownChangeLocation();
        })

    }, "jsonp");

    $('#vpnModal').bind('show.bs.modal', function(e) {
        var $target = $(e.relatedTarget);
        var location = $target.attr('data-location');
        $('#vpnModalLocation').html(
            location.split(',').map(function(loc) {
                var state = window.getState(loc);
                if (state) {
                    return '<b>' + state.country.split(',')[0] + '</b>';
                }
            }).join(', ')
        );
    })

    function fillPredictApps(count, location) {
        var data = window.predictApps
        var template  = $.templates("#offer-template");
        var data = sortDataByLocation(data, location);
        data = data.slice(0, count);

        $('#offersContainer').html(template.render(data));
    }


    function sortDataByLocation(data, country) {
        return data
            ? data
                .sort(function(a, b) {
                    var aLoc = a.Location
                        .split(', ')
                        .map(function(n) { return n.trim()});
                    var bLoc = a.Location
                        .split(', ')
                        .map(function(n) { return n.trim()});

                    if (aLoc.indexOf(country) >= 0) return -1;
                    else if (bLoc.indexOf(country) >=0) return 1;
                    else return 1;
                })
            : []
    }

    function dropdownChangeLocation() {
        var data = window.predictApps;
        var locations = data.reduce(function(all, n) {
            return all.concat(
                n.Location
                    .split(',')
                    .map(function(l) {return l.trim()}
                )
            )
        }, [])
        locations = locations.filter(function(b, i) {
            return locations.indexOf(b) === i;
        });
        locations = locations.concat(window.currentCountry);
        var $container = $('<div class="row"><div class="col-sm-4 col-sm-offset-4"><select class="form-control" name="loc"></select></div></div>');
        var $select = $container.find('select');
        locations.forEach(function(loc) {
            $select.append('<option value="' + loc + '">' + loc + '</option>');
        });

        $container.insertAfter('#offersContainer');
        $select.val(window.currentCountry)

        $select.bind('change', function() {
            fillPredictApps(6, $(this).val())
        });
    }

    function modifyPredicAppIsAvailable(data) {
        data.forEach(function(pa) {
            pa.isAvailableLocation = pa.Location
                .split(',')
                .map(function(l) {
                    return l.trim().toLowerCase();
                })
                .indexOf(window.currentCountry) !== -1;
        });
    }
});

