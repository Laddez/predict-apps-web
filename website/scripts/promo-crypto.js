$(function() {
    var currentData = null;
    var predictionData = null;

    $('body').bind('crypto.GetData', function() {    
        $.get('https://api.coinmarketcap.com/v1/ticker/', function(result) {
            currentData = result;

            return Promise.all([
                $.get('http://predictapps.com/predict.php?Name=' + result[0].symbol + '&Price=' + result[0].price_usd), 
                $.get('http://predictapps.com/predict.php?Name=' + result[1].symbol + '&Price=' + result[1].price_usd), 
                $.get('http://predictapps.com/predict.php?Name=' + result[2].symbol + '&Price=' + result[2].price_usd),
            ])
            .then(function(res) {
                    res.forEach(function (item){
                        var predict = item.Results.output1.value.Values[0];
                        var targetItem = currentData.find(function(n) {
                            return n.symbol === predict[0];
                        })
                        targetItem.prediction = predict[2];                   
                    })
            })
            .then(function() {               
                fillPromoSelect('.js-promo-hero', 3);
                fillPromoSelect('.js-promo-prediction', 3);
                fillTable(3);
            })
        });
    })

    $('body').bind('crypto.FillTable', function() {
        fillTable(3)
    });
    
    var $cryptoTable = $('#cryptotable tbody');
    var $moreLink = $('#more-link');
    var $lessLink = $('#less-link');

    var primaryColor = "#27CE7D";
    var secondaryColor = "#f44336";
    
    

    $moreLink.bind('click', function() {
        fillTable(10);
        $moreLink.css('display', 'none');
        $lessLink.css('display', 'block');
    });
    $lessLink.bind('click', function() {
        fillTable(3);
        $lessLink.css('display', 'none');
        $moreLink.css('display', 'block');
    });


    function getPrice(now, perc){
        now = parseFloat(now);
        perc = parseFloat(perc);

        var result = null;
        var difference = now * Math.abs(perc) / 100;

        if (perc > 0) {
            result = now + difference;
        } else {
            result = now - difference;
        }
        return result;
    }

    function getPerc(prediction, now){
        now = parseFloat(now);
        prediction = parseFloat(prediction);

        var result = ((prediction * 100) / now) - 100;
        return result;
    }

    function percColor(value) {
        return (value >= 0) ? primaryColor : secondaryColor;
    }

    function addSign(value) {
        return (value > 0) ? ('+' + value) : value;
    }

    function roundFloat(value, num) {
        num = Math.pow(10, num);
        var result = Math.ceil(value*num)/num;
        return result;
    }

    function fillPromoSelect(selector, num) {
        var data = currentData.slice(0, num);
        var $container = $(selector);
        var $select = $container.find('.js-promo-select');

        data.forEach(function(item) {
            $select.append('<option value="' + item.rank +'">'+ item.name +'</option>')
        });
        fillPromoData(selector, currentData[0]);

        $select.bind('change', function() {
            var selectedId = parseInt($(this).val());
    
            var currentItem = currentData.find(function(item) {
                return item.rank == selectedId;
            })
            fillPromoData(selector, currentItem);
        })
    }
    
    function fillPromoData(selector, current) {
        var $container = $(selector);
        
        var changeDayUSD = roundFloat(getPrice(current.price_usd, current.percent_change_24h), 2);
        //---------------------- current.prediction
        var predictionPerc = roundFloat(getPerc(current.prediction, current.price_usd), 1);

        $container.find('.js-promo-current').text("$" + roundFloat(current.price_usd, 2));
        $container.find('.js-promo-change-day').text("$" + changeDayUSD);
        $container.find('.js-promo-change-day-perc').text(addSign(roundFloat(current.percent_change_24h, 1)) + '%')
        .css('color', percColor(current.percent_change_24h));
        //---------------------- current.prediction
        $container.find('.js-promo-prediction').text("$" + roundFloat(current.prediction, 2));
        $container.find('.js-promo-prediction-perc').text(addSign(predictionPerc, 1) + '%')
        .css('color', percColor(predictionPerc));
    }

    function fillTable(num) {
        var data = null;
        $cryptoTable.empty();
        if (num) {
            data = currentData.slice(0, num);
        } else {
            data = currentData;
        }

        data.forEach(function(item) {
            var predApp = window.predictApps.find(function(pa) {
                    return pa.Crypto == item.name
                });
            $cryptoTable.append(
                '<tr>' +
                '<td data-title="Currency">' + item.name +'</td>' + 
                '<td data-title="USD">' + '$' + roundFloat(item.price_usd, 2) +'</td>' +
                '<td data-title="BTC">' + roundFloat(item.price_btc, 5) +'</td>' +
                '<td data-title="Change for 24h"><span class="text-perc" style="color:' + percColor(item.percent_change_24h) + '">' + 
                addSign(roundFloat(item.percent_change_24h, 1)) + '%' +'</span></td>' + 
                '<td data-title="Change for 1h"><span class="text-perc" style="color:' + percColor(item.percent_change_1h) + '">' + 
                addSign(roundFloat(item.percent_change_1h, 1))  + '%' +'</span></td>' + 
                '<td>' +
                (predApp ? '<a href="' + predApp.Link + '" class="btn btn-primary btn__sm" target="_blank">Download App</a>' + 
                '<div style="height:20px;" class="d-block d-sm-none"></div>' : '') + 
                '</td>' +
                '</tr>'
            )
        });
    }
    
})