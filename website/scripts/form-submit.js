$(function() {
    $('#phoneNumber').mask('PN#', {'translation': {
        P: {pattern: /[+\d]/},
        N: {pattern: /\d/}
      }
    });

    $('#contactForm').submit(function() {
        var $form = $(this);
        var $modal = $('#contactModal');

        if ($form.valid()) {
            var formData = $form.serialize();
            $.ajax({
                type: 'POST',
                url: $form.attr('action'),
                data: formData,
                success: function(data) {
                    $('#contactForm').trigger('reset');
                    setTimeout(function() { $modal.modal(); }, 500);
                    setTimeout(function() { $modal.modal('hide'); }, 5000);
                },
                error: function(xhr, str) {
                    console.log('Error: ' + xhr.responseCode);
                }
            });
        }

        return false;
    })
})